package models

object Role {
  val USER = "user"
  val ADMIN = "admin"
}